# SSC5955 - Final Project

Final Project of the course SCC5955 Autonomous Systems, Optimization, Decision-Making: Algorithms ans Applications.

## Exercise 1
#### AUV initial position - GREEN waypoint
#### Xinitial = 7
#### Yinitial = 3
#### Zinitial = 1
#### AUV final position - RED waypoint
#### Xgoal = 1.0
#### Ygoal = 4.0
#### Zgoal = 9.5
![Left Image](ex1.png)

## Exercise 2
#### AUV initial position - GREEN waypoint
#### Xinitial = 4
#### Yinitial = 7
#### Zinitial = 4
#### AUV final position - RED waypoint
#### Xgoal = 3.0
#### Ygoal = 1.0
#### Zgoal = 0.5
![Left Image](ex2.png)

## Exercise 3
#### AUV initial position - GREEN waypoint
#### Xinitial = 1
#### Yinitial = 7
#### Zinitial = 4
#### AUV final position - RED waypoint
#### Xgoal = 1.0
#### Ygoal = 1.0
#### Zgoal = 0.0
![Left Image](ex3.png)

## Exercise 4
#### AUV initial position - GREEN waypoint
#### Xinitial = 8
#### Yinitial = 1
#### Zinitial = 5
#### AUV final position - RED waypoint
#### Xgoal = -2.0
#### Ygoal = 4.0
#### Zgoal = 2.5
![Left Image](ex4.png)
