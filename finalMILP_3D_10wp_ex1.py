#######################################################################
# FINAL PROJECT:
# Non-convex optimization for path planning
# by Victor Hugo Sillerico Justo - USP nro. 11904461
#######################################################################

# get Gurobi library to work with python
from gurobipy import *

# extra libraries for visualization
import matplotlib.pyplot as plt
import numpy as np
import mpl_toolkits.mplot3d.art3d as art3d
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.transforms import Bbox
from matplotlib.path import Path
from itertools import product, combinations
from matplotlib.patches import Rectangle, PathPatch

# define a model
mdl = Model('RAP')

# parameters for the non-convex optimization process
deltaT=1.0
f=1.0
m=1.0
# define the "BIG M"
M=20.0
# define the obstacle (only CUBE like)
a1=2.0
b1=6.0
a2=a1
b2=b1
# AUV initial position - GREEN waypoint
Xinitial = 7
Yinitial = 3
Zinitial = 1
# AUV final position - RED waypoint
Xgoal = 1.0
Ygoal = 4.0
Zgoal = 9.5
# set of time steps
times =['1','2','3','4','5','6','7','8','9','10']
# auxiliar variable to get the last position available in the "times" vector
lastPos = len(times)-1

# variables of position
x = mdl.addVars(times,name='x',lb=-10.0,ub=10.0)
y = mdl.addVars(times,name='y',lb=-10.0,ub=10.0)
z = mdl.addVars(times,name='z',lb=-10.0,ub=10.0)
# variables of velocity
vx = mdl.addVars(times,name='vx',lb=-10.0,ub=10.0)
vy = mdl.addVars(times,name='vy',lb=-10.0,ub=10.0)
vz = mdl.addVars(times,name='vz',lb=-10.0,ub=10.0)
# variables of acceleration
aX = mdl.addVar(name='aX',lb=-10.0,ub=10.0)
aY = mdl.addVar(name='aY',lb=-10.0,ub=10.0)
aZ = mdl.addVar(name='aZ',lb=-20.0,ub=20.0)
# auxiliar variables
aux_X = mdl.addVar(name='aux_X')
aux_Y = mdl.addVar(name='aux_Y')
aux_Z = mdl.addVar(name='aux_Z')
# binary variables
z1 = mdl.addVars(times, vtype=GRB.BINARY, name='z1')
z2 = mdl.addVars(times, vtype=GRB.BINARY, name='z2')
z3 = mdl.addVars(times, vtype=GRB.BINARY, name='z3')
z4 = mdl.addVars(times, vtype=GRB.BINARY, name='z4')
# dynamic constraints: position
mdl.addConstrs((x[times[t+1]]==x[times[t]]+vx[times[t]]*deltaT+0.5*(deltaT**2)*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='xpos')
mdl.addConstrs((y[times[t+1]]==y[times[t]]+vy[times[t]]*deltaT+0.5*(deltaT**2)*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='ypos')
mdl.addConstrs((z[times[t+1]]==z[times[t]]+vz[times[t]]*deltaT+0.5*(deltaT**2)*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='zpos')
# dynamic constraints: velocity
mdl.addConstrs((vx[times[t+1]]==vx[times[t]]+deltaT*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='vx')
mdl.addConstrs((vy[times[t+1]]==vy[times[t]]+deltaT*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='vy')
mdl.addConstrs((vz[times[t+1]]==vz[times[t]]+deltaT*f/m for t in range(len(times)) if times[t] != times[lastPos]), name='vz')
# initial constraints
mdl.addConstrs((x[times[t]]==Xinitial for t in range(len(times)) if times[t]==times[0]), name = 'x0')
mdl.addConstrs((y[times[t]]==Yinitial for t in range(len(times)) if times[t]==times[0]), name = 'y0')
mdl.addConstrs((z[times[t]]==Zinitial for t in range(len(times)) if times[t]==times[0]), name = 'z0')
# final constraints
mdl.addConstr((aX==Xgoal-x[times[lastPos]]),name='objX')
mdl.addConstr((aY==Ygoal-y[times[lastPos]]),name='objY')
mdl.addConstr((aZ==Zgoal-z[times[lastPos]]),name='objZ')

mdl.addConstr((aux_X>=aX),name='obj-X1')
mdl.addConstr((aux_X>=-1*aX),name='obj-X2')
mdl.addConstr((aux_Y>=aY),name='obj-Y1')
mdl.addConstr((aux_Y>=-1*aY),name='obj-Y2')
mdl.addConstr((aux_Z>=aZ),name='obj-Z1')
mdl.addConstr((aux_Z>=-1*aZ),name='obj-Z2')
# obstacle avoidance constraints
mdl.addConstrs((x[times[t]]<=a1+z1[times[t]]*M for t in range(len(times))), name='obs_left')
mdl.addConstrs((x[times[t]]>=b1-z2[times[t]]*M for t in range(len(times))), name='obs_right')
mdl.addConstrs((y[times[t]]<=a2+z3[times[t]]*M for t in range(len(times))), name='obs_down')
mdl.addConstrs((y[times[t]]>=b2-z4[times[t]]*M for t in range(len(times))), name='obs_up')
mdl.addConstrs((z1[times[t]]+z2[times[t]]+z3[times[t]]+z4[times[t]]<=3 for t in range(len(times))), name='obs_int')
# objective function
obj=aux_X+aux_Y+aux_Z
# optimization step
mdl.setObjective(obj,GRB.MINIMIZE)
mdl.optimize()

###############################################################################
# print results
mdl.printAttr('x')
w=[]
for t in range(len(times)):
    temp_X=float(x[times[t]].X)
    temp_Y=float(y[times[t]].X)
    temp_Z=float(z[times[t]].X)
    w.append([temp_X,temp_Y,temp_Z])
print(w)

###############################################################################
# 3D visualization
vertices=[]
left, bottom, width, height = (a1, a2, b1-a1, b2-a2)
rect = plt.Rectangle((left, bottom), width, height, facecolor="black", alpha=0.1)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# plot the waypoints GREEN:initial - RED:goal - BLUE:intermediate
for t in range(len(times)):
    temp_X=float(x[times[t]].X)
    temp_Y=float(y[times[t]].X)
    temp_Z=float(z[times[t]].X)
    if t==0:
        ax.scatter(temp_X, temp_Y, temp_Z, s=40, c='g', marker='o', linewidths=0.2)
    elif t==lastPos:
        ax.scatter(temp_X, temp_Y, temp_Z, s=40, c='r', marker='o', linewidths=0.2)
    else:
        ax.scatter(temp_X, temp_Y, temp_Z, s=40, c='b', marker='o', linewidths=0.2)
ax.set_aspect("equal")
# plot the cube obstacle
r = [a1, b1]
for s, e in combinations(np.array(list(product(r, r, r))), 2):
    if np.sum(np.abs(s-e)) == r[1]-r[0]:
        ax.plot3D(*zip(s, e), color="r")
colors = ['b', 'g', 'r', 'c', 'm', 'y']
for i, (z, zdir) in enumerate(product([a1,b1], ['x','y','z'])):
    side = Rectangle((a1, a1), b1-a1, b1-a1, facecolor=colors[i])
    ax.add_patch(side)
    art3d.pathpatch_2d_to_3d(side, z=z, zdir=zdir)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()
